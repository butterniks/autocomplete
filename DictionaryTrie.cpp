#include "util.hpp"
#include "DictionaryTrie.hpp"

/* Create a new Dictionary that uses a Trie back end */
DictionaryTrie::DictionaryTrie(){
  root = NULL;
}

/* Insert a word with its frequency into the dictionary.
 * Return true if the word was inserted, and false if it
 * was not (i.e. it was already in the dictionary or it was
 * invalid (empty string) */
bool DictionaryTrie::insert(std::string word, unsigned int freq)
{

  Node* oldNode;
  Node* currentNode = root;
  int i = 0;
  while(i<word.size()){
    if(root == NULL){
      root = new Node(word[i]); 
      currentNode = root;
    }
    else if(currentNode->value < word[i]){
      if(currentNode->left == NULL){
        currentNode->left = new Node(word[i]);

      }
      currentNode = currentNode->left;
    }
    else if(currentNode->value > word[i]){
      if(currentNode->right == NULL){
        currentNode->right = new Node(word[i]);
      }
      currentNode = currentNode->right;
    }
    else{
      i++;
      if(i < word.size()){
        if(currentNode->mid == NULL){ 

          currentNode->mid = new Node(word[i]);
        }
        currentNode = currentNode->mid;
      }

    }
  }
  if(currentNode->marker==false){
    currentNode->marker = true;
    currentNode->frequency = freq;
    return true;
  }

  return false;

}

/* Return true if word is in the dictionary, and false otherwise */
bool DictionaryTrie::find(std::string word) const
{
  Node* currentNode = root;

  DictionaryTrie::findLastNode(word, currentNode);

  if(currentNode == 0 ||currentNode->marker ==false){
    return false;
  }

  return true;
}

void DictionaryTrie::findLastNode(std::string word, Node*& currentNode) const {
  int i = 0;
  while(i<word.size()){
    if(currentNode == NULL){
      return;
    }
    else if(currentNode->value < word[i]){
      currentNode = currentNode->left;
    }
    else if(currentNode->value > word[i]){
      currentNode = currentNode->right;
    }
    else{
      if(i != word.size()-1){
        currentNode = currentNode->mid;
      }
      i++;
    }
  }
}



/* Return up to num_completions of the most frequent completions
 * of the prefix, such that the completions are words in the dictionary.
 * These completions should be listed from most frequent to least.
 * If there are fewer than num_completions legal completions, this
 * function returns a vector with as many completions as possible.
 * If no completions exist, then the function returns a vector of size 0.
 * The prefix itself might be included in the returned words if the prefix
 * is a word (and is among the num_completions most frequent completions
 * of the prefix)
 */
std::vector<std::string> DictionaryTrie::predictCompletions(std::string prefix, unsigned int num_completions)
{
  std::vector<std::string> words;
  Node* currentNode = root;
  
  //check for empty prefix
  if(prefix == ""){
    return words;
  }
  
  //finding starting node
  DictionaryTrie::findLastNode(prefix, currentNode);
  if(currentNode == 0){
    return words;
  }

  std::priority_queue<std::pair<int, std::string>> queue;
  if(currentNode->marker == true){
    queue.push(std::pair<int, std::string>(currentNode->frequency, prefix));
  }
  currentNode = currentNode->mid;
  DictionaryTrie::depthSearch(prefix, queue, currentNode);
  
  int i  = 0;
  while(!queue.empty() && i < num_completions){
    words.push_back(queue.top().second);
    queue.pop();
    i++;
  }


  return words;
}

void DictionaryTrie::depthSearch(std::string str, std::priority_queue<std::pair<int, std::string>>& v, Node* curr){
  if(curr == NULL){
    return;
  }

  if(curr->marker == true){
    std::pair <int, std::string> p(curr->frequency, str + curr->value);
    v.push(p);

  }
  //recurse left, right, mid
  depthSearch(str, v, curr->left);
  depthSearch(str + curr->value, v, curr->mid);
  depthSearch(str, v, curr->right);
}

/* Destructor */
DictionaryTrie::~DictionaryTrie(){
  deleteAll(root);
}

void DictionaryTrie::deleteAll(Node* n){
  if(n == NULL){
    return;
  }
  else{
    deleteAll(n->left);
    deleteAll(n->mid);
    deleteAll(n->right);
    delete(n);
  }
}


