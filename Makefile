
CC=g++
CXXFLAGS=-std=c++11 -g -gdwarf-2 -Wall
LDFLAGS=-g

all: benchtrie util.o

benchtrie: util.o  DictionaryTrie.o DictionaryBST.o DictionaryHashtable.o

DictionaryTrie.o: DictionaryTrie.hpp 

DictionaryBST.o: DictionaryBST.hpp

DictionaryHashtable.o: DictionaryHashtable.hpp

Tester: DictionaryBST.o DictionaryTrie.o DictionaryHashtable.o 

util.o: util.hpp

clean:
	rm -f benchtrie *.o *~ core*
CC=g++
CXXFLAGS=-std=c++11 -g -gdwarf-2 -Wall
LDFLAGS=-g

all: benchtrie util.o

benchtrie: util.o  DictionaryTrie.o DictionaryBST.o DictionaryHashtable.o

DictionaryTrie.o: DictionaryTrie.hpp 

DictionaryBST.o: DictionaryBST.hpp

DictionaryHashtable.o: DictionaryHashtable.hpp

Tester: DictionaryBST.o DictionaryTrie.o DictionaryHashtable.o 

util.o: util.hpp

clean:
	rm -f benchtrie *.o *~ core*  
