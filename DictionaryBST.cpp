#include "util.hpp"
#include "DictionaryBST.hpp"

/* Create a new Dictionary that uses a BST back end */
DictionaryBST::DictionaryBST(){
  DictionaryBST::bst = new std::set<std::string>();
}

/* Insert a word into the dictionary. */
bool DictionaryBST::insert(std::string word)
{
  return std::get<1>(DictionaryBST::bst->insert(word));
}

/* Return true if word is in the dictionary, and false otherwise */
bool DictionaryBST::find(std::string word) const
{
  return DictionaryBST::bst->find(word) != DictionaryBST::bst->end();
}

/* Destructor */
DictionaryBST::~DictionaryBST(){
  delete(DictionaryBST::bst);
}
